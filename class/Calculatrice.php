<?php


class Calculatrice
{
    public function calcul($entry = "2+3")
    {
        return $this->calculate($entry);
    }

    private function calculate($data)
    {
        $data_array = str_split($data);
        // La racine carrée
        if(count($data_array) == 7){
            $data_chiffre = $data_array[5];
            $racine = array( 1 => "sqrt");
            $chiffre = array( 2 => $data_chiffre);
            $data_array = array_replace($racine, $chiffre);
        }
        dd($data_array);
        return $this->get_operation($data_array);
    }

    public function get_operation($array)
    {   
        switch ($array[1]) {
            case "+": // addition
                return $this->addition($array[0], $array[2]);
                break;
            case "-": // soustraction
                return $this->soustraction($array[0], $array[2]);
                break;
            case "*": // multiplication
                return $this->multiplication($array[0], $array[2]);
                break;
            case "/": // division
                return $this->division($array[0], $array[2]);
                break;
            case "%": // modulo
                return $this->modulo($array[0], $array[2]);
                break;
            case "sqrt": // racine carrée
                return $this->racine($array[2]);
                break;   
        } 
    }

    public function addition($chiffre_1, $chiffre_2)
    {
        return (int)$chiffre_1 + (int)$chiffre_2;
    }

    public function soustraction($chiffre_1, $chiffre_2)
    {
        return (int)$chiffre_1 - (int)$chiffre_2;
    }

    public function multiplication($chiffre_1, $chiffre_2)
    {
        return (int)$chiffre_1 * (int)$chiffre_2;
    }

    public function division($chiffre_1, $chiffre_2)
    {
        return (int)$chiffre_1 / (int)$chiffre_2;
    }

    public function modulo($chiffre_1, $chiffre_2)
    {
        return (int)$chiffre_1 % (int)$chiffre_2;
    }

    public function racine($chiffre_1)
    {
        return sqrt($chiffre_1);
    }


}